This is a library to model a loop service where the commit on a source code repository will be put on a reward proposal in a smart contract and according to the contract rules and the actively participating community, it could be rewarded shares of the backing tokens.

```
   ____________________
  /                    \
  |  commit generator  |
  |       engine       |           __________________
  \____________________/          /                  \
            |                     |  smart contract  |
            |                     |     engine       |
     _______|_______              \__________________/
    /               \                      |
    |    service    |______________________|
    |    engine     |
    \_______________/
            |
            |
       _____|_____
      |           |
      |     c     |
      |     l     |_____
      |     i     |     |
      |     e     |     |
      |     n     |     |
      |     t     |     |
      |___________|     |
            |           |
            |           |
            |___________|

```
