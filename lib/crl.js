"use strict";

var me = module.exports = {}

var e = me.engine = {}

var ec = e.commit = {}

var er = e.reward = {}

var el = e.loop = {}

function Commit(option) {
    this.hash = option.hash;
    this.author = option.author;
    this.data = option.data;
    this.message = option.message
}

Commit.prototype.toString = function() {
    return this.hash
}

function commitFactory(option) {
    return new Commit(option)
}


function CommitEngine(option) {
    const crypto = option.crypto;
    this.hookHandlerFactory = option.hookHandlerFactory;
    this.storage = option.storage;
    this.connector = option.connector;

    // routes to hookhandlers
    this.router = option.router
}

CommitEngine.prototype.start = function(option) {
    // start the required listenners
    //establish connectivity with the loop
}

CommitEngine.prototype.stop = function(option) {
    // close connections
    // remove listenners
    //exit clean
}

CommitEngine.prototype.setHook = function(option) {
    // if option is array it should be a set of endpoints
}

CommitEngine.prototype.addCommit = function(option) {
    // inform the loop
    // inform the swarn
    // save into storage as an open item
}


function HookHandler(option) {

    const hkey = option.crypto.key;

    const sign = option.crypto.lib.createSign(option.crypto.signWith);

    this.keywork = function(option) {

        if (option.sign && option.buffer) {
            sign.update(option.buffer);
            return sign.sign(hkey, 'hex')
        }

        if (option.dec && option.buffer) {
            return option.crypto.lib.publicDecrypt(hkey, option.buffer)
        }

        if (option.enc && option.buffer) {
            return option.crypto.lib.publicEncrypt(hkey, option.buffer)
        }

    };

    this.crypto = {
        cert: option.crypto.cert,
        remote: {
            validate: option.crypto.validateRemote || false
        }
    };

    this.endpoint = option.endpoint;

    this.router = {};

    this.extractor = option.extractor

}

HookHandler.prototype.getRouter = function() {
  return this.router;
}

HookHandler.prototype.attach = function(option, cb) {
    // option should pass in an running socket(CommitEngine connector), the endpoint path and the router?
    //console.log('HookHandler.prototype.attach', option)

    option.socket.on('request', hookFactory(option))
    return cb && cb(null, option.socket);
};

HookHandler.prototype.spawn = function(option, cb) {
    // option should pass in an interface and the port for the new socket
    // the endpoint and a factory for the router

    const hhserver = option.https.createServer(option.crypto, hookFactory(option)).listen(option.port)

    return cb && cb(null, hhserver)

};

function hookHandlerFactory(option) {
    return new HookHandler(option)
};


function hookFactory(option) {
    // options should have a structure that will hold all the events with each callback
    // or just a callback that will be called in each case

    // use post for hooks with payload
    // we could use get for informative hooks (not on github spec though)

    // here we have to find a way for reconfiguring later

    return function httpsCreateServerHandler(req, res) {
        // headers to collect : X-GitHub-Event and X-GitHub-Delivery
        // first guess is to check for action key inside the payload
        //console.log(option.router, req.url)
        const router = option.router;
        const routeIndex = Object.keys(router).indexOf(req.url);

        const selectedKeys =  routeIndex > -1 ? router[req.url].hookHeaderKeys.filter(function(key) {
            return -1 !== Object.keys(req.headers).indexOf(key)
        }) : null;

        const contentLength = parseInt(req.headers['content-length']);

        if (routeIndex > -1 && selectedKeys) {

            const data = [];

            req.on('data', function(chunk) {
                data.push(chunk)
            });

            req.on('end', function() {
                const actualLength = data.reduce(function(sum, item) {
                    sum = sum + item.length;
                    return sum
                }, 0);
                    option.handler(null, data.join())
                    //res.setHeader('Set-Cookie', ['type=ninja', 'language=javascript']);
                    //res.setHeader('Content-Type', 'application/json');
                    //res.setHeader('Location', 'https://www.systemics.gr/' + req.url );
                    //res.setHeader('Vary', 'Upgrade-Insecure-Requests');
                    res.setHeader('Content-Length', 0);
                    res.writeHead(200);
                    res.end();


            });

        } else {

            res.writeHead(400);
            res.end()

        }

    }

}


ec.hookHandlerFactory = hookHandlerFactory;

ec.createFactory = commitFactory
