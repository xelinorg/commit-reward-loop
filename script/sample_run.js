const crl = require('../lib/crl.js')
const crypto = require('crypto');
const ocsp = require('ocsp');
const fs = require('fs');
const path = require('path');
const https = require('https');

var cache = new ocsp.Cache();

localwrite = function(name, b){

  fs.writeFile(name, b,  "binary",function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("The " + name + "file was saved!");
    }
  });

}

const opt = {
  crypto : {
    key: fs.readFileSync(path.resolve('./test/crypto/sec.xelin.org/account.key')),
    cert: fs.readFileSync(path.resolve('./test/crypto/sec.xelin.org/0005_chain.pem')),
    //ca: [ fs.readFileSync(path.resolve('./test/crypto/chain.pem'))],
    dhparam: fs.readFileSync(path.resolve('./test/crypto/dhparam.pem')),
    //ocsp: fs.readFileSync(path.resolve('./systemics.gr.ocsp')),
    lib: crypto,
    honorCipherOrder:true,
    requestCert: true,
    rejectUnauthorized: true,
    requestOCSP: true,
    //secureProtocol: 'TLSv1_method',
    signWith: 'RSA-SHA256'
  }
}

const spawnOpt = {
  https: https,
  crypto: opt.crypto,
  interface: "0.0.0.0",
  port: 443,
  router: {
        "/hello": {
            hookHeaderKeys: ['X-Gitlab-Event'],
            hookHeaderValues: ['Push Hook', 'Issue Hook']
        }
  },
  handler: function(error, data) {
    console.log('endpoint root', error, data)
  }
};


function handleOCSR(xcert, xissuer, xcb){
  const ocspHandle = new tls.TLSSocket({
    socket:
    isServer: false,
    requestCert: true,
    rejectUnauthorized: true,
    requestOCSP: true
  });

  ocspHandle.on('OCSPResponse', function(res){
    console.log('ocspresponse>>>', res)
  })

  ocspHandle.on('secureConnect', function(){
    console.log('secure connect>>>', arguments)
  })

  ocspHandle.on('error', function(){
    console.log('error>>>', arguments)
  })

}


const webhook = crl.engine.commit.hookHandlerFactory(opt);

webhook.spawn(spawnOpt, function(error, serverHandler) {

  if (error) {console.log(error);};

  serverHandler.addContext('sec.xelin.org', spawnOpt.crypto)

  serverHandler.on('secureConnection', function(data) {
      console.log('on secureConnection start>>>')
      console.log(data)
      console.log('<<<on secureConnection end')


  });

  serverHandler.on('clientError', function(error) {
      console.log('on clientError', error)
  });

  serverHandler.on('close', function() {
    console.log('serverHandler closing...', error)
    //socket.destroy();
  });

  serverHandler.on('newSession', function() {
    console.log('serverHandler newSession...', arguments)
  });

  serverHandler.on('OCSPRequest', function(xcert, xissuer, xcb) {
    console.log('serverHandler OCSPRequest...')
    ocsp.getOCSPURI(xcert, function(err, uri) {
      if (err) return xcb(err);
      if (uri === null) return xcb();
      var req = ocsp.request.generate(xcert, xissuer);
      cache.probe(req.id, function(err, cached) {

        if (err) return xcb(err);
        if (cached !== false) {
          return xcb(null, cached.response);
        }

        var options = {
          url: uri,
          ocsp: req.data
        };

        cache.request(req.id, options, xcb);
      });
    });

  });

  serverHandler.on('resumeSession', function() {
    console.log('serverHandler resumeSession...', arguments[0].toString())
    arguments[1](null, null)
  });

  serverHandler.on('tlsClientError', function() {
    console.log('serverHandler tlsClientError...', arguments)
  });



  serverHandler.on('connection', function(socket) {
    console.log(socket.remoteAddress)
    console.log(socket.localAddress)
  });

});
