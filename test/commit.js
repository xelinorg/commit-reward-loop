
describe('testing the commit engine', function(){
  let option,
    commit;

  beforeEach(function() {

    option = {
      hash: '1234567890',
      author: 'a@a.a',
      data: 'data string',
      message: 'message string'
    };

    commit = crl.engine.commit.createFactory(option);

  });

  it('it should test commitFactory is returning the expected commit instance', function(done){

    should(commit.hash).be.equal(option.hash);
    should(commit.author).be.equal(option.author);
    should(commit.data).be.equal(option.data);
    should(commit.message).be.equal(option.message);
    done()

  });

  it('it should test commit toString return the commit hash', function(done){

    should(commit.toString()).be.equal(option.hash);
    done()

  });

});
