const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

global.crl = require("../")
global.https = require('https');

// when mocha is fired up by npm test, the process's working directory is the
// project root. There should be a over solution to path handling
global.crypto = {
    key: fs.readFileSync(path.resolve('./test/crypto/crloop.key.pem')),
    cert: fs.readFileSync(path.resolve('./test/crypto/crloop.crt.pem')),
    ca: fs.readFileSync(path.resolve('./test/crypto/rootCA.crt.pem')), 
    //dhparam: fs.readFileSync(path.resolve('./test/crypto/dhparam.pem')),
    lib: crypto,
    signWith: 'RSA-SHA256'
}

global.runningSockets = {}
