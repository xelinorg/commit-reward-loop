describe('testing the hookhandler on the commit engine', function() {
    let option,
        webhook;
    const testMessage = 'the broken telephone is ringing';
    const meetingport = 8888;
    const meetingPath = '/gitpub';
    const router = {};
    router[meetingPath] = {
        hookHeaderKeys: ['X-Gitlab-Event'],
        hookHeaderValues: ['Push Hook', 'Issue Hook']
    };
    const requestOpt = {
        hostname: 'server.crl',
        port: meetingport,
        path: meetingPath,
        method: 'POST',
        key: crypto.key,
        cert: crypto.cert,
        ca: crypto.ca
    };
    const testPayload01 = {
        "object_kind": "push",
        "before": "95790bf891e76fee5e1747ab589903a6a1f80f22",
        "after": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
        "ref": "refs/heads/master",
        "checkout_sha": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
        "user_id": 4,
        "user_name": "niaou gab",
        "user_username": "niaougab",
        "user_email": "niaou gab@a.a",
        "user_avatar": "",
        "project_id": 9,
        "project": {
            "id": 9,
            "name": "commit-reward-loop",
            "description": "",
            "web_url": "",
            "avatar_url": null,
            "git_ssh_url": "",
            "git_http_url": "",
            "namespace": "xelinorg",
            "visibility_level": 0,
            "path_with_namespace": "xelinorg/commit-reward-loop",
            "default_branch": "master",
            "homepage": "",
            "url": "git@gitlab.com:xelinorg/commit-reward-loop.git",
            "ssh_url": "git@gitlab.com:xelinorg/commit-reward-loop.git",
            "http_url": "http://gitlab.com/xelinorg/commit-reward-loop.git"
        },
        "repository": {
            "name": "commit-reward-loop",
            "url": "git@gitlab.com:xelinorg/commit-reward-loop.git",
            "description": "",
            "homepage": "http://gitlab.com/xelinorg/commit-reward-loop.git",
            "git_http_url": "http://gitlab.com/xelinorg/commit-reward-loop.git.git",
            "git_ssh_url": "git@gitlab.com:xelinorg/commit-reward-loop.git.git",
            "visibility_level": 0
        },
        "commits": [{
                "id": "b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
                "message": "Added reward for the commits",
                "timestamp": "2018-06-20T14:27:31+02:00",
                "url": "http://gitlab.com/xelinorg/commit-reward-loop/commit/b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
                "author": {
                    "name": "A user",
                    "email": "a@a.a"
                },
                "added": ["CHANGELOG"],
                "modified": ["test/hookhandler.js"],
                "removed": []
            },
            {
                "id": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
                "message": "fixed README",
                "timestamp": "2018-06-20T23:36:29+02:00",
                "url": "http://gitlab.com/xelinorg/commit-reward-loop/commit/da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
                "author": {
                    "name": "B user",
                    "email": "b@db.b"
                },
                "added": ["CHANGELOG"],
                "modified": ["README.md"],
                "removed": []
            }
        ],
        "total_commits_count": 2
    };

    function gitpubMockRequest(byTitle) {
        return function gitpubResponseHandler(res) {
            let data = '';

            res.on('data', function(chunk) {
                data += chunk;
            });

            res.on('end', function() {
                runningSockets[byTitle].close();
            });

            res.on('error', function(error) {
                runningSockets[byTitle].close();
            });
        };

    };


    beforeEach(function() {

        option = {
            crypto: crypto,
            endpoint: {
                hash: meetingPath,
                connector: https
            }
        };

        webhook = crl.engine.commit.hookHandlerFactory(option);

    });

    it('it should test hookhandler crypto settings', function(done) {

        should(webhook.crypto.cert).be.equal(crypto.cert);
        should(webhook.crypto.remote.validate).be.equal(false);
        should.not.exist(webhook.crypto.key);
        done();

    });

    it('it should test hookhandler endpoint settings', function(done) {

        should(webhook.endpoint.connector).be.equal(option.endpoint.connector);
        should(webhook.endpoint.hash).be.equal("/gitpub");
        done();

    });

    it('it should test hookhandler keywork sign', function(done) {

        const signOpt = {
            sign: true,
            buffer: Buffer.from(testMessage)
        };

        const sign = option.crypto.lib.createSign(option.crypto.signWith);
        sign.update(signOpt.buffer);
        const expectedSignedMessage = sign.sign(option.crypto.key, 'hex');

        const singedMessage = webhook.keywork(signOpt);


        should(singedMessage).be.equal(expectedSignedMessage);

        done();

    });

    it('it should spawn the hookhandler', function(done) {

        const ctx = this.test;

        const spawnOpt = {
            https: https,
            crypto: crypto,
            interface: "server.crl",
            port: meetingport,
            router: router,
            handler: function(error, data) {
                const actual = JSON.parse(data);
                should(actual.object_kind).be.equal('push');
                should(actual.project_id).be.equal(9);
                done();
            }
        };

        webhook.spawn(spawnOpt, function(error, serverHandler) {
            if (error) {
              return done(error)
            }
            runningSockets[ctx.title] = serverHandler;
            //process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
            const req = https.request(requestOpt, gitpubMockRequest(ctx.title));
            req.setHeader('X-Gitlab-Event', 'Push Hook');
            req.end(JSON.stringify(testPayload01));
        });

    });

    it('it should attach the hookhandler', function(done) {

        const ctx = this.test;

        const attachOpt = {
            router: router,
            socket: https.createServer(option.crypto).listen(meetingport),
            handler: function(error, data) {
                const actual = JSON.parse(data);
                should(actual.object_kind).be.equal('push');
                should(actual.project_id).be.equal(9);
                done();
            }
        };


        webhook.attach(attachOpt, function(error, serverHandler) {
            if (error) {
              return done(error)
            }
            //process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
            runningSockets[ctx.title] = serverHandler;
            const req = https.request(requestOpt, gitpubMockRequest(ctx.title));
            req.setHeader('X-Gitlab-Event', 'Push Hook');
            req.end(JSON.stringify(testPayload01));
        });

    });
});
