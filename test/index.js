
describe('testing library loading', function(){
  var token='hell',
  counter=0;

  beforeEach(function() {
    token = token + counter.toString();
    counter++
  });

  it('it should expose the library', function(done){
    crl.should.have.property('engine')
    crl.engine.should.have.property('commit')
    crl.engine.should.have.property('reward')
    crl.engine.should.have.property('loop')
    done();
  });

  it('it should dispose the library', function(done){
    token.toString().should.equal('hell01')
    done();
  });
});
